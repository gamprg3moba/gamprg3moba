﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionMovementAccel : MonoBehaviour
{
    [SerializeField]
    private DirectionMovement directionControl;
    [SerializeField]
    private Vector3 inputDirection = Vector3.zero;
    [SerializeField]
    private float maxSpeed = 50;
    [SerializeField]
    private float curSpeed = 0f;
    [SerializeField]
    private float accel = 1f;
    [SerializeField]
    private float dccel = 2f;
    private bool stopAccel = false;

    public Vector3 InputDirection { get => inputDirection; set => inputDirection = value; }

    // Start is called before the first frame update
    void Start()
    {
        if (!directionControl)
            directionControl = GetComponent<DirectionMovement>();
    }
    private void FixedUpdate()
    {
        //TODO FIX THIS!!!
        if (inputDirection.magnitude > 0f){
            curSpeed += accel * Time.fixedDeltaTime;
        }
        else if(curSpeed > 0f ){
            curSpeed -= dccel * Time.fixedDeltaTime;
        }
        curSpeed = Mathf.Clamp(curSpeed, 0f, maxSpeed);
        directionControl.Speed = curSpeed;
    }
}
