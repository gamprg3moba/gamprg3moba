﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionMovement : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction = Vector3.zero;
    [SerializeField]
    private float speed = 0f;
    [SerializeField]
    protected bool ignoreX = false;
    [SerializeField]
    protected bool ignoreY = false;
    [SerializeField]
    protected bool ignoreZ = false;

    public Vector3 Direction { get => direction; set => this.direction = value; }
    public float Speed { get => speed; set => speed = value; }
    public bool IgnoreX { get => ignoreX;}
    public bool IgnoreY { get => ignoreY;}
    public bool IgnoreZ { get => ignoreZ;}

    public Vector3 VectoredSpeed() {
        Vector3 dir = this.direction;
        dir.Set(dir.x * (ignoreX ? 0f : 1f), dir.y * (ignoreY ? 0f : 1f), dir.z * (ignoreZ ? 0f : 1f));
        
        return dir * speed;
    }

    private void SetDirection(Vector3 dir) {
        if (dir.magnitude > 0f) {
            this.direction = dir;
        }
    }
}
