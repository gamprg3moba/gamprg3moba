﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    private Transform playerObject;
    [SerializeField]
    private float targRotation = 0f;
    [SerializeField]
    private float rotationSpeed = 180f;
    // Start is called before the first frame update
    void Start()
    {
        playerObject = this.transform;
        targRotation = GetRotation();
    }

    private void FixedUpdate()
    {
        float x = 0f;
        float rotation = GetRotation();
        float rotateCalc = Mathf.SmoothDampAngle(rotation, this.targRotation, ref x, Time.deltaTime) * Mathf.Deg2Rad;
        this.playerObject.forward = new Vector3(Mathf.Cos(rotateCalc), 0f, Mathf.Sin(rotateCalc));
    }

    public float GetRotation() {
        Vector3 rotation = playerObject.forward;
        return Mathf.Atan2(rotation.z, rotation.x)* Mathf.Rad2Deg;
    }

    public void TargetDirection(Vector3 target) {
        Vector3 angleDelta = target - this.playerObject.position;
        targRotation = Mathf.Atan2(angleDelta.z, angleDelta.x) * Mathf.Rad2Deg;
    }


}
