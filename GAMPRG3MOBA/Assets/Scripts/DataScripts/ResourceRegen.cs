﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public abstract class ResourceRegen : MonoBehaviour
{
    [SerializeField]
    protected float baseMax,baseMin, max, cur;
    [SerializeField]
    protected float regen = 0.1f;
    public UnityEvent onEmpty;
    public FloatEvent onConsume, onRestore;
    public GameObjectEvent onFullRestore;

    public float BaseMax { get => baseMax;}
    public float BaseMin { get => baseMin; set => baseMin = value; }
    public float Max { get => Mathf.Max(this.baseMin, this.max); set => max = value; }
    public float Cur { get => cur;  }
    public float Regen { get => regen; set => regen = value; }

    // Start is called before the first frame update
    void Start()
    {

    }


    private void FixedUpdate()
    {
        Restore(this.regen);
    }

    public void SetValues(float max) {
        this.max = this.baseMax = max;
        this.cur = this.Max;
    }

    public float GetNormalValue() {
        return cur / Max;
    }

    public bool CheckConsume(float value) {
        return cur >= value;
    }

    public void IncreaseBy(float x) {
        float baseMax = this.max;
        this.max += x;
        this.cur *= this.Max / baseMax;
    }

    public void Consume(float value) {
        if (!CheckConsume(value)) {
            value = cur;
        }
        cur -= value;
        if(value > 0)
            onConsume.Invoke(value);
        if (cur <= 0)
            onEmpty.Invoke() ;
    }

    public void Restore(float value) {
        if (value > (max - cur)) {
            value = max - cur;
        }
        cur += value;
        if(value >0)
            onRestore.Invoke(value);
    }

    public void FullRestore() {
        cur = this.Max;
        onFullRestore.Invoke(this.gameObject);
    }

}
