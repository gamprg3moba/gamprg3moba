﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct RangedValue {
    private float min, max, value;
    private bool overflow;

    public float Min { get => min; set => min = value; }
    public float Max { get => max; set => max = value; }
    public float Value { get => value; set => ClampValue(value); }

    public void ClampValue(float value) {
        this.value = value;
        ClampValueMinMax();
    }

    private void ClampValueMinMax() {
        if(!overflow)
            value = Mathf.Min(this.value, max);
        value = Mathf.Max(this.value, min);
    }
    public void FixMinMax() {
        if (max < min) {
            float temp = min;
            min = max;
            max = temp;
        }
    }

}

[RequireComponent(typeof(HealthResource))]
[RequireComponent(typeof(ManaResource))]
public class Stats : MonoBehaviour
{
    [SerializeField]
    private BaseStats baseStats;
    private HealthResource hp;
    private ManaResource mn;

    private void Reset()
    {
        hp = GetComponent<HealthResource>();
        mn = GetComponent<ManaResource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Reset();
        hp.SetValues(200f + 20f * baseStats.Strength);
        mn.SetValues(75f + 12f * baseStats.Intelligence);
    }

    private void FixedUpdate()
    {
        
    }
}
