﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "CharacterData/BaseStats")]
public class BaseStats : ScriptableObject
{
    [SerializeField]
    private int strength,agility,intelligence;

    public int Strength { get => strength;}
    public int Agility { get => agility;}
    public int Intelligence { get => intelligence; }
}
