﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchChangeCam : MonoBehaviour
{
    [SerializeField]
    private Camera cam;
    public float revolutionSpeed;
    public float dir = 0f;
    // Start is called before the first frame update
    void Start()
    {
        if (!cam)
            cam = GetComponent<Camera>();
    }

    private void FixedUpdate()
    {
        //TEST CODE
        dir = Input.GetAxis("Mouse Y");
        this.transform.eulerAngles += Vector3.right * revolutionSpeed * dir;
        this.transform.eulerAngles.Set(Mathf.Clamp(this.transform.eulerAngles.x, 0f, 90f),
            this.transform.eulerAngles.y,
            this.transform.eulerAngles.z);
    }


}
