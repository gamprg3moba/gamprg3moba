﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScroll : MonoBehaviour
{
    [SerializeField]
    private string scrollAxis = "Mouse ScrollWheel";
    [SerializeField]
    private float dir;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update(){
        dir = Input.GetAxis(scrollAxis);
    }
}
