﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableCam : DirectionMovement
{
    [SerializeField]
    private TargetCam cam;


    // Start is called before the first frame update
    void Start()
    {
        if (cam == null)
            cam = GetComponent<TargetCam>();
    }

    private void FixedUpdate(){
        cam.target += this.VectoredSpeed()* Time.fixedDeltaTime;
    }

    public void SetDirection2D(Vector2 dir) {
        this.Direction = new Vector3(dir.x, 0, dir.y);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
