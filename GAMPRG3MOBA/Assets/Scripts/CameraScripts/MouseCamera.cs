﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Vector2Event : UnityEvent<Vector2> { };

public class MouseCamera : MonoBehaviour
{
    [SerializeField]
    private float moveThreshold = 0.9f;
    [SerializeField]
    private MovableCam moveCam;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        Vector2 mousePosition = GetMousePosition();
        float x, y;
        x = y = 0;
        if (Mathf.Abs(mousePosition.x) >= moveThreshold || Mathf.Abs(mousePosition.y) >= moveThreshold) {
            x = mousePosition.x;
            y = mousePosition.y;
        }
        mousePosition.Set(x, y);
        mousePosition.Normalize();
        moveCam.SetDirection2D(mousePosition);
    }

    private void Update()
    {
    }

    public Vector2 GetMousePosition() {
        Vector2 mousePosition = Input.mousePosition;
        mousePosition.Set(mousePosition.x / Screen.width, mousePosition.y / Screen.height);
        mousePosition -= new Vector2(0.5f, 0.5f);
        mousePosition *= 2f;
        return mousePosition;
    }
}
