﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct FloatRange {
    //this struct is for ranged values
    [SerializeField]
    private float value;
    [SerializeField]
    private float min;
    [SerializeField]
    private float max;

    public float Value { get => value; set => SetValue(value); }
    public float Min { get => min; set => SetMin(value); }
    public float Max { get => max; set => SetMax(value); }

    private void SetValue(float x) {
        value = Mathf.Min(Mathf.Max(x, min), max);
    }
    private void SetMin(float x) {
        this.min = Mathf.Min(x, this.max);
    }
    private void SetMax(float x) {
        this.max = Mathf.Max(x, this.min);
    }
    private void SwapMinMax() {
        float temp = min;
        min = max;
        max = temp;
    }
    public FloatRange SetMinMax(float min, float max) {
        this.min = min;
        this.max = max;
        if (!(this.min < this.max) )
            this.SwapMinMax();
        return this;
    }

}

public class TargetCam : MonoBehaviour
{
    public Vector3 target = Vector3.zero;
    public FloatRange cameraRange = new FloatRange() { Max = 40f, Min = 20f, Value = 30f };

    public float frameDelay = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate(){
        //movement
        if (frameDelay <= 0f)
            this.transform.position = -this.transform.forward * cameraRange.Value + target;
        else {
            Vector3 vel = Vector3.zero;
            this.transform.position = Vector3.SmoothDamp(this.transform.position, -this.transform.forward * cameraRange.Value + target, 
                ref vel, Time.fixedDeltaTime * frameDelay);
        }
    }

    // Update is called once per frame
    void Update(){
        //zoom using scrollwheel
        float dir = Input.GetAxis("Mouse ScrollWheel");
        this.cameraRange.Value = this.cameraRange.Value - dir*10f;
    }

    private void OnDrawGizmos(){
        Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * cameraRange.Value);
        Gizmos.DrawWireSphere(this.transform.position + this.transform.forward * cameraRange.Value, 2.5f);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(target + Vector3.forward * 0.5f, target - Vector3.forward * 0.5f);
        Gizmos.DrawLine(target + Vector3.right * 0.5f, target - Vector3.right * 0.5f);
    }


}
