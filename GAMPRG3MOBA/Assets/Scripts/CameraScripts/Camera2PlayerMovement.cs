﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

[System.Serializable]
public class VectorEvent : UnityEvent<Vector3> { };

public class Camera2PlayerMovement : MonoBehaviour
{
    public PlayerRotation playerRote;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1) && playerRote) {
            playerRote.TargetDirection(GetMouse2World());
        }
    }

    public Vector3 GetMouse2World() {
        RaycastHit hit;
        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 location = Vector3.zero;
        if (Physics.Raycast(r, out hit)) {
            location = hit.point;
        }
        return location;
    }
}
