﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimits : MonoBehaviour
{
    // Start is called before the first frame update
    public Collider area;
    private TargetCam targetCam;
    void Start()
    {
        if (targetCam == null)
            targetCam = GetComponent<TargetCam>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (targetCam.target.x > area.bounds.extents.x)
            targetCam.target.x = area.bounds.extents.x;
        else if (targetCam.target.x < -area.bounds.extents.x)
            targetCam.target.x = -area.bounds.extents.x;

        if (targetCam.target.y > area.bounds.extents.y)
            targetCam.target.y = area.bounds.extents.y;
        else if (targetCam.target.y < -area.bounds.extents.y)
            targetCam.target.y = -area.bounds.extents.y;

        if (targetCam.target.z > area.bounds.extents.z)
            targetCam.target.z = area.bounds.extents.z;
        else if (targetCam.target.z < -area.bounds.extents.z)
            targetCam.target.z = -area.bounds.extents.z;

    }
}
