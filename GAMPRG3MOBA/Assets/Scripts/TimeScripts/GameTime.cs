﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTime : MonoBehaviour
{
    public static GameTime instance = null;

    [SerializeField]
    private float gameTimer = 0f;
    private float resumeSpeed = 0f;

    public float GameTimer { get => gameTimer;}

    private void Awake()
    {
        if (!instance)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        this.gameTimer += Time.fixedDeltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSpeed(float speed) {
        Time.timeScale = speed;
    }

    public void ResetSpeed() {
        Time.timeScale = 1f;
    }
    public void StopSpeed() {
        this.resumeSpeed = Time.timeScale;
        Time.timeScale = 0f;
    }
    public void ResumeSpeed(){
        Time.timeScale = this.resumeSpeed;
    }

}
