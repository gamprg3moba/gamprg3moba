﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycle : MonoBehaviour
{
    [SerializeField]
    private Light targLight;
    [SerializeField]
    private float changeRate = 10f;
    [SerializeField]
    private float baseValue = 0f;
    [SerializeField]
    private int segments = 2;

    [SerializeField]
    private float rotate = 0f;
    
    
    void Start()
    {
        baseValue = targLight.transform.eulerAngles.x;
    }

    private void FixedUpdate()
    {
        
        float time = GameTime.instance.GameTimer;
        float inc = Mathf.Floor(time / changeRate) % segments;
        rotate = baseValue + inc * (360f / segments);
        targLight.transform.rotation = Quaternion.Euler(rotate, 135,
            0);
        

        



    }
}
