﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ResourceRegen))]
public class ResourceRegenEditor : TargetEditorScript<ResourceRegen>
{
    private float value = 1f;
    public override void OnInspectorGUI()
    {
        CheckForTarget();
        base.OnInspectorGUI();
        value = EditorGUILayout.FloatField("Consume/restore amount", this.value);
        if (GUILayout.Button("Consume/Restore")) {
            ConsumeRestore();
        }
    }

    public void ConsumeRestore() {
        if (value > 0)
            targetScript.Restore(value);
        else
            targetScript.Consume(-1 * value);
    }
}

[CustomEditor(typeof(HealthResource))]
public class HealthEditor : ResourceRegenEditor { }

[CustomEditor(typeof(ManaResource))]
public class ManaEditor : ResourceRegenEditor { }
