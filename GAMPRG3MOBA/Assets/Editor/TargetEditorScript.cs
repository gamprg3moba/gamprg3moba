﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public abstract class TargetEditorScript<T>: Editor
    where T:MonoBehaviour
{
    protected T targetScript = null;

    protected void CheckForTarget() {
        if (targetScript == null)
            targetScript = (T)this.target;
    }
}
